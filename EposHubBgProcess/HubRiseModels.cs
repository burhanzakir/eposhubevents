﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EposHubBgProcess
{
    public class HubRiseCallbackEvent
    {
        public string id { get; set; }
        public string resource_type { get; set; }
        public string event_type { get; set; }
        public DateTime created_at { get; set; }
        public string customer_id { get; set; }
        public string resource_id { get; set; }
        public string customer_list_id { get; set; }
        public string access_token { get; set; }
        public bool isProcessed { get; set; }
        public Station station { get; set; }
    }
    public class HubRiseAccessToken
    {
        public string access_token { get; set; }
        public string location_id { get; set; }
        public string catalog_id { get; set; }
        public string customer_list_id { get; set; }
    }
    public class HubRiseAccessTokenReq
    {
        public string code { get; set; }
        public string client_id { get; set; }
        public string client_secret { get; set; }
    }
    public class HubRiseMenuResponse
    {
        public string id { get; set; }
        public string account_id { get; set; }
        public string location_id { get; set; }
        public string name { get; set; }
        public DateTime created_at { get; set; }
        public HubRiseData data { get; set; }
    }
    public class HubRiseMenu
    {
        public HubRiseData data { get; set; }
    }
    public class HubRiseData
    {
        public List<HubRiseCategories> categories { get; set; }
        public List<HubRiseOptionLists> option_lists { get; set; }
        public List<HubRiseProducts> products { get; set; }
        public List<HubRiseDiscounts> discounts { get; set; }
        public List<HubRiseDeal> deals { get; set; }
    }
    public class HubRiseCategories
    {
        public string name { get; set; }
        public string @ref { get; set; }
        public string description { get; set; }
        public List<string> tags { get; set; }
    }
    public class HubRiseOptionLists
    {
        public string @ref { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public List<HubRiseOption> options { get; set; }
    }
    public class HubRiseOption
    {
        public string name { get; set; }
        public string @ref { get; set; }
        public string price { get; set; }
        public List<string> tags { get; set; }
    }
    public class HubRiseProducts
    {
        public string name { get; set; }
        public string description { get; set; }
        public string category_ref { get; set; }
        public List<string> tags { get; set; }
        public List<string> image_ids { get; set; }
        public List<HubRiseSku> skus { get; set; }
    }
    public class HubRiseSku
    {
        public string name { get; set; }
        public string @ref { get; set; }
        public string price { get; set; }
        public int position { get; set; }
        public List<string> option_list_refs { get; set; }
    }
    public class HubRiseDiscounts { }
    public class HubRiseDeal
    {
        public string name { get; set; }
        public string @ref { get; set; }
        public string description { get; set; }
        public List<string> image_ids { get; set; }
        public List<HubRiseLine> lines { get; set; }
    }
    public class HubRiseLine
    {
        public string pricing_effect { get; set; }
        public string pricing_value { get; set; }
        public List<HubRiseLineSku> skus { get; set; }
        public string label { get; set; }
    }
    public class HubRiseLineSku
    {
        public string @ref { get; set; }
    }

    /*********************************/
    public class ImageData
    {
        public string name { get; set; }
        public string Id { get; set; }
        public string @type { get; set; }
        public string md5 { get; set; }
        public byte[] binary { get; set; }
    }

    public class HubRiseCallBackOption
    {
        public string @ref { get; set; }
        public string name { get; set; }
        public string option_list_name { get; set; }
        public string price { get; set; }
    }

    public class HubRiseCallBackItem
    {
        public string sku_ref { get; set; }
        public string price { get; set; }
        public string quantity { get; set; }
        public string subtotal { get; set; }
        public string tax_rate { get; set; }
        public string customer_notes { get; set; }
        public string product_name { get; set; }
        public string sku_name { get; set; }
        public string points_earned { get; set; }
        public string points_used { get; set; }
        public List<HubRiseCallBackOption> options { get; set; }
    }

    public class HubRiseCallBackLoyaltyOperation
    {
        public string name { get; set; }
        public string delta { get; set; }
        public string reason { get; set; }
        public DateTime? created_at { get; set; }
        public string new_balance { get; set; }
    }

    public class HubRiseCallBackPayment
    {
        public string @ref { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public string amount { get; set; }
        public HubRiseCardInfo info { get; set; }
    }

    public class HubRiseCardInfo
    {
        public string pay_id { get; set; }
        public string acceptance { get; set; }
        public string status { get; set; }
        public string nc_status { get; set; }
        public string nc_error { get; set; }
        public string nc_error_plus { get; set; }
        public string eci { get; set; }
        public string amount { get; set; }
        public string payment_method { get; set; }
        public string brand { get; set; }
        public string card_number { get; set; }
        public string cardholder_name { get; set; }
    }

    public class HubRiseCallBackLoyaltyCard
    {
        public string id { get; set; }
        public string customer_id { get; set; }
        public string name { get; set; }
        public string @ref { get; set; }
        public string balance { get; set; }
    }

    public class HubRiseCallBackCustomer
    {
        public string id { get; set; }
        public string customer_list_id { get; set; }
        public string private_ref { get; set; }
        public string email { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string gender { get; set; }
        public string birth_date { get; set; }
        public string company_name { get; set; }
        public string phone { get; set; }
        public string address_1 { get; set; }
        public string address_2 { get; set; }
        public string postal_code { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string delivery_notes { get; set; }
        public bool sms_marketing { get; set; }
        public bool email_marketing { get; set; }
        public int nb_orders { get; set; }
        public string order_total { get; set; }
        public DateTime? first_order_date { get; set; }
        public DateTime? last_order_date { get; set; }
        public List<HubRiseCallBackLoyaltyCard> loyalty_cards { get; set; }
    }

    public class HubRiseCallBackNewState
    {
        public string id { get; set; }
        public string location_id { get; set; }
        public string private_ref { get; set; }
        public string status { get; set; }
        public string service_type { get; set; }
        public DateTime? created_at { get; set; }
        public string created_by { get; set; }
        public DateTime? expected_time { get; set; }
        public DateTime? confirmed_time { get; set; }
        public string customer_notes { get; set; }
        public List<string> coupon_codes { get; set; }
        public string total { get; set; }
        public string total_discrepancy { get; set; }
        public string payment_discrepancy { get; set; }
        public List<HubRiseCallBackItem> items { get; set; }
        public List<HubRiseCallBackLoyaltyOperation> loyalty_operations { get; set; }
        public List<Charge> charges { get; set; }
        public List<HubRiseCallBackPayment> payments { get; set; }
        public List<string> discounts { get; set; }
        public HubRiseCallBackDeals deals { get; set; }
        public HubRiseCallBackCustomer customer { get; set; }
    }
    public class Charge
    {
        public string charge_type { get; set; }
        public string charge_price { get; set; }
        public object charge_ref { get; set; }
    }
    public class HubRiseCallBackDeals
    { }
    public class HubRiseCallBackOrder
    {
        public string id { get; set; }
        public string resource_type { get; set; }
        public string event_type { get; set; }
        public DateTime? created_at { get; set; }
        public string order_id { get; set; }
        public string resource_id { get; set; }
        public string location_id { get; set; }
        public HubRiseCallBackNewState new_state { get; set; }
    }

    public class ResponseRegisterUser
    {
        public int CustomerID { get; set; }
        public int AddressID { get; set; }
    }


    /****** New Customer event***/
    
    //public class HubRiseCustomerNewState
    //{
    //    public string id { get; set; }
    //    public string customer_list_id { get; set; }
    //    public string private_ref { get; set; }
    //    public string email { get; set; }
    //    public string first_name { get; set; }
    //    public string last_name { get; set; }
    //    public string gender { get; set; }
    //    public string birth_date { get; set; }
    //    public string company_name { get; set; }
    //    public string phone { get; set; }
    //    public string address_1 { get; set; }
    //    public string address_2 { get; set; }
    //    public string postal_code { get; set; }
    //    public string city { get; set; }
    //    public string state { get; set; }
    //    public string country { get; set; }
    //    public string latitude { get; set; }
    //    public string longitude { get; set; }
    //    public string delivery_notes { get; set; }
    //    public bool sms_marketing { get; set; }
    //    public bool email_marketing { get; set; }
    //    public int nb_orders { get; set; }
    //    public string order_total { get; set; }
    //    public string first_order_date { get; set; }
    //    public string last_order_date { get; set; }
    //    public List<string> loyalty_cards { get; set; }
    //}

    public class HubRiseCustomerCallback
    {
        public string id { get; set; }
        public string resource_type { get; set; }
        public string event_type { get; set; }
        public DateTime created_at { get; set; }
        public string customer_id { get; set; }
        public string resource_id { get; set; }
        public string customer_list_id { get; set; }
        public HubRiseCallBackCustomer new_state { get; set; }
    }

}
