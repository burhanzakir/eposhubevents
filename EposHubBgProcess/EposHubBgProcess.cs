﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net.Http;
using Newtonsoft.Json;

namespace EposHubBgProcess
{
    public class EposHubBgProcessController
    {
        private static string EPOSHUB_BASE_URL = ""; // http://13dcrawfordstreet.no-ip.info:8123/";
        private static string EPOSHUB_APP_URL = ""; // "EposHub.SssPosWebHooks.svc/";
        private static string HUBRISE_BASE_URL = ""; // "https://api.hubrise.com/v1"; //+"/catalogs/zjg2z/images";        
        private static int _timerInMilliseconds = 20000;
        private static Timer _timer;

        public EposHubBgProcessController()
        {

            EPOSHUB_BASE_URL = Properties.Settings.Default.EposHubBaseUrlSetting;
            EPOSHUB_APP_URL = Properties.Settings.Default.EposHubAppUrlSetting;
            HUBRISE_BASE_URL = Properties.Settings.Default.HubriseBaseUrlSetting;
            _timerInMilliseconds = (Properties.Settings.Default.TimerInSeconds == 0) ? 20000 : Properties.Settings.Default.TimerInSeconds * 1000;

            _timer = new Timer(_ => DoHubRiseDuty(), null, 0, Timeout.Infinite);
        }


        public static async void DoHubRiseDuty()
        {
            //Console.WriteLine("Hello at " + DateTime.Now.TimeOfDay);
            Log("DoHubRiseDuty-" + DateTime.Now.TimeOfDay);
            try
            {
                using (EposHubDbDataContext dc = new EposHubDbDataContext())
                {
                    var stations = (from s in dc.sp_StationsChkMenuChanged(true) select s).ToList();

                    if (stations != null)
                    {
                        foreach (var station in stations)
                        {
                            string companyName = GetCompanyName(station.ClientUD.ToString());
                            if (string.IsNullOrEmpty(companyName)) continue;
                            if (await HubRiseUploadCatalog("delivery", station.ClientUD.ToString(), companyName)) // why delivery?
                            {
                                station.IsMenuChanged = false;
                                dc.SubmitChanges();
                            }
                        }
                    }
                    await TriggerCallBacks();
                }
                _timer.Change(_timerInMilliseconds, Timeout.Infinite);
            }
            catch(Exception e)
            {
                Log("****DoHubRiseDuty-Exception" + e.Message);
                _timer.Change(_timerInMilliseconds, Timeout.Infinite);
            }
        }

        private static async Task TriggerCallBacks()
        {
            Log("TriggerCallBacks-" + DateTime.Now.TimeOfDay);
            var husbRiseCallBackEvents = await GetHubRiseCallbackEvents();
            foreach (var e in husbRiseCallBackEvents)
            {
                if (await ProcessHubRiseEvent(e))
                {
                    var deletedEvent = await DeleteEvent(e);
                }
            }
        }

        private static string GetCompanyName(string clientID)
        {
            SqlConnection adminConn = new SqlConnection("Password=SoftSolution5;Persist Security Info=True;User ID=EposUser;Initial Catalog=EposAdmin;Data Source=109.228.2.43");
            StringBuilder adminQuery = new StringBuilder();
            string result = "";

            adminQuery.Clear();
            adminQuery.Append("SELECT c.Name FROM Companies c ");
            adminQuery.Append("JOIN Clients s ON s.CompanyID =  c.CompanyID ");
            adminQuery.Append("WHERE s.ClientID = '" + clientID + "'");

            DataTable adminData = new DataTable();
            SqlCommand adminCommand = new SqlCommand(adminQuery.ToString(), adminConn);
            using (SqlDataAdapter da = new SqlDataAdapter(adminCommand))
            {
                da.Fill(adminData);
            }

            foreach (DataRow row in adminData.Rows)
            {
                result = row["Name"].ToString();
            }
            return result;
        }

        private static async Task<List<HubRiseCallbackEvent>> GetHubRiseCallbackEvents()
        {

            Log("GetHubRiseCallbackEvents-" + DateTime.Now.TimeOfDay);

            List<HubRiseCallbackEvent> callBackEvents = new List<HubRiseCallbackEvent>();

            using (EposHubDbDataContext dc = new EposHubDbDataContext())
            {
                var stations = (from s in dc.sp_StationsChkMenuChanged(false) select s).ToList();
                if (stations == null) return null;
                foreach (var station in stations)
                {
                    Dictionary<string, string> headers = new Dictionary<string, string>();
                    headers.Add("Content-Type", "application/json");
                    headers.Add("X-Access-Token", station.HubRiseAccessToken);
                    var hubRiseEvent = await ApiClient.CallApi<List<HubRiseCallbackEvent>>(HUBRISE_BASE_URL, "callback/events", null, headers, RestApiCallType.GET, false);

                    if (hubRiseEvent != null)
                    {
                        foreach (var e in hubRiseEvent)
                        {
                            e.access_token = station.HubRiseAccessToken;
                            e.station = station;
                        }
                        callBackEvents.AddRange(hubRiseEvent);
                    }
                }
            }

            return callBackEvents;
        }

        private static async Task<object> DeleteEvent(HubRiseCallbackEvent e)
        {

            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("Content-Type", "application/json");
            headers.Add("X-Access-Token", e.access_token);
            return await ApiClient.CallApi<object>(HUBRISE_BASE_URL, "callback/events/" + e.id, null, headers, RestApiCallType.DELETE, false);
        }

        private static async Task<bool> ProcessHubRiseEvent(HubRiseCallbackEvent e)
        {
            if (e == null) return false;
            try
            {
                switch (e.resource_type)
                {
                    case "order":
                        if (e.event_type == "create")
                        {
                            Log("Order-Create-" + e.id);
                            Dictionary<string, string> headers = new Dictionary<string, string>();
                            headers.Add("Content-Type", "application/json");
                            headers.Add("X-Access-Token", e.access_token);
                            var hROrder = await ApiClient.CallApi<HubRiseCallBackOrder>(HUBRISE_BASE_URL, "callback/events/" + e.id, null, headers, RestApiCallType.GET, false);
                            var orderId = await HubRiseProcessOrder(hROrder, e);
                            if (orderId == null) return false;
                            Log("Order-Created-OrderId=" + orderId);
                            if (orderId == "-1") return false;
                            return true;
                        }
                        else if (e.event_type == "update") // ?
                        {
                            // do nothing, i think!
                            return true;
                        }
                        break;
                    case "customer":
                        if (e.event_type == "create")
                        {
                            Log("Customer-Create-" + e.id);
                            Dictionary<string, string> headers = new Dictionary<string, string>();
                            headers.Add("Content-Type", "application/json");
                            headers.Add("X-Access-Token", e.access_token);
                            var customer = await ApiClient.CallApi<HubRiseCustomerCallback>(HUBRISE_BASE_URL, "callback/events/" + e.id, null, headers, RestApiCallType.GET, false);
                            var registeredIDJson = await RegisterUser(e.station, customer.new_state);
                            var registerResponse = JsonConvert.DeserializeObject<ResponseRegisterUser>(registeredIDJson);
                            if (registerResponse == null) return false;
                            if (registerResponse.CustomerID < 0) return false;
                            if (!string.IsNullOrEmpty(customer.new_state.address_1) && !string.IsNullOrEmpty(customer.new_state.postal_code) && registerResponse.CustomerID < 0) return false;
                            Log("Customer-Created-CustomreId=" + registerResponse.CustomerID);
                            return true;
                        }
                        else if (e.event_type == "update") // ?
                        {
                            Dictionary<string, string> headers = new Dictionary<string, string>();
                            headers.Add("Content-Type", "application/json");
                            headers.Add("X-Access-Token", e.access_token);
                            var customer = await ApiClient.CallApi<HubRiseCustomerCallback>(HUBRISE_BASE_URL, "callback/events/" + e.id, null, headers, RestApiCallType.GET, false);
                            var registeredIDJson = await RegisterUser(e.station, customer.new_state);
                            var registerResponse = JsonConvert.DeserializeObject<ResponseRegisterUser>(registeredIDJson);
                            if (registerResponse == null) return false;
                            if (registerResponse.CustomerID < 0) return false;
                            if (!string.IsNullOrEmpty(customer.new_state.address_1) && !string.IsNullOrEmpty(customer.new_state.postal_code) && registerResponse.CustomerID < 0) return false;
                            return true;
                        }
                        break;
                }
                return false;
            }
            catch(Exception ee)
            {
                Log("****ProcessHubRiseEvent-Exception" + ee.Message);
                return false;
            }
        }

        private static async Task<string> RegisterUser(Station s, HubRiseCallBackCustomer c)
        {
            OnlineCustomer onlineCustomer = new OnlineCustomer();
            onlineCustomer.Customer = new Customer
            {
                ClientUD = s.ClientUD,
                FirstName = c.first_name,
                LastName = c.last_name,
                Telephone = c.phone,
                Username = c.email,
                Password = Encoding.UTF8.GetBytes("hubRise")
            };
            if (string.IsNullOrEmpty(c.address_1) || string.IsNullOrEmpty(c.postal_code))
            {
                onlineCustomer.Address = null;
            }
            else
            {
                onlineCustomer.Address = new CustomerAddress
                {
                    AddressLine1 = c.address_1,
                    AddressLine2 = c.address_2,
                    AddressName = "",
                    IsBillingAddress = true,
                    Postcode = c.postal_code,
                    TownOrCity = c.city
                };
            }
            
            var response =  await ServicePost("RegisterCustomer", onlineCustomer);
            if (response.ToString() == "-1")
                return JsonConvert.SerializeObject(new ResponseRegisterUser { AddressID = -1, CustomerID = -1 });
            return response;
        }

        private static async Task<string> ServicePost(string method, object parameter)
        {
            try
            {
                string requestUrl = EPOSHUB_BASE_URL + EPOSHUB_APP_URL + method;
                var payload = JsonConvert.SerializeObject(parameter);
                Log("URL:" + requestUrl);
                Log("Payload" + payload);

                // Removed use of HttpClient and used class ApiClient  - BZK 18/05/04 

                // HttpClient httpC = new HttpClient();
                // var result = await httpC.PostAsync(requestUrl, new StringContent(payload, Encoding.UTF8, "application/json"));
                //Log("Status code " + result.StatusCode.ToString());
                //Log("ReasonPhrase " + result.ReasonPhrase.ToString());
                //var content = await result.Content.ReadAsStringAsync();
                //Log("Content " + content);

                //if (result.StatusCode == System.Net.HttpStatusCode.OK)
                //{
                //    return JsonConvert.DeserializeObject<string>(await result.Content.ReadAsStringAsync());
                //}

                Dictionary<string, string> headers = new Dictionary<string, string>();
                headers.Add("Content-Type", "application/json");
                var result = await ApiClient.CallApi<string>(EPOSHUB_BASE_URL + EPOSHUB_APP_URL, method, payload, headers, RestApiCallType.POST, false);
                if (!string.IsNullOrWhiteSpace(result))
                    return result;

                return "-1";
            }
            catch { return "-1"; }
        }

        private static async Task<string> HubRiseProcessOrder(HubRiseCallBackOrder o, HubRiseCallbackEvent e)
        {
            Log("HubRiseProcessOrder-" + DateTime.Now.TimeOfDay);
            try
            {
                var registeredIDJson = await RegisterUser(e.station, o.new_state.customer);
                Log("Registered User:" + registeredIDJson);
                var registerResponse = JsonConvert.DeserializeObject<ResponseRegisterUser>(registeredIDJson);
                Log("Serialised OK"); 
                SPosOrderEvent sPosOrderEvent = new SPosOrderEvent
                {
                    @event = "NEW_ORDER",
                    location_id = e.station.ProviderLocationID,
                    order = new SPosOrder
                    {
                        order_id = o.order_id,
                        name = o.new_state.created_by + " Order ",
                        asap = (o.new_state.expected_time == null) ? "true" : "false",
                        order_type = o.new_state.service_type,
                        pickup_at = o.new_state.expected_time?.TimeOfDay.ToString(),
                        total = Convert.ToInt32(Convert.ToDecimal(o.new_state.total.Replace(" GBP", string.Empty)) * 100),
                        currency = "GBP",
                        notes = o.new_state.customer.delivery_notes,
                        items = new List<SPosItem>(),
                        OrderJSON = JsonConvert.SerializeObject(o),
                        is_collection = (o.new_state.service_type == "collection") ? true : false,
                        is_paid = false
                    },
                    acknowledged_at = "",
                    payment_records = new List<SPosPayment>(),
                    customer = new OnlineCustomer
                    {
                        Customer = new Customer
                        {
                            ClientUD = e.station.ClientUD,
                            CustomerID = Convert.ToInt32(registerResponse.CustomerID),
                            FirstName = o.new_state.customer.first_name,
                            LastName = o.new_state.customer.last_name,
                            Password = Encoding.UTF8.GetBytes(""),
                            Telephone = o.new_state.customer.phone,
                            Username = o.new_state.customer.email
                        },
                        Address = new CustomerAddress
                        {
                            AddressID = registerResponse.AddressID,
                            AddressLine1 = o.new_state.customer.address_1,
                            AddressLine2 = o.new_state.customer.address_2,
                            AddressName = "",
                            Postcode = o.new_state.customer.postal_code,
                            TownOrCity = o.new_state.customer.city,
                            IsBillingAddress = true,
                            CustomerID = Convert.ToInt32(registerResponse.CustomerID)
                        }
                    }

                };
                decimal paymentRecordsSum = 0;
                foreach (var pRecord in o.new_state.payments)
                {
                    if (pRecord.type != "cash")
                        paymentRecordsSum += Convert.ToDecimal(pRecord.amount.Replace(" GBP", string.Empty));
                }
                if (Convert.ToDecimal(o.new_state.total.Replace(" GBP", string.Empty)) == paymentRecordsSum)
                    sPosOrderEvent.order.is_paid = true;

                foreach (var item in o.new_state.items)
                {
                    var posI = new SPosItem
                    {
                        pos_item_name = item.product_name,
                        //pos_item_id = item.ProductID, // variant ID haitham was: item.variantid,
                        pos_item_id = Convert.ToInt32(item.sku_ref),
                        unit_price = (int)(Convert.ToDecimal(item.subtotal.Replace(" GBP", string.Empty)) * 100),
                        quantity = Convert.ToInt32(Convert.ToDecimal(item.quantity)),
                        modifiers = new List<SPosModifier>()
                    };
                    foreach (var mod in item.options)
                    {
                        posI.modifiers.Add(new SPosModifier
                        {
                            pos_item_id = (mod.@ref == null) ? 0 : Convert.ToInt32(mod.@ref),
                            unit_price = (int)(Convert.ToDecimal(mod.price.Replace(" GBP", string.Empty)) * 100),
                            mod_name = mod.name,
                        });
                    }
                    sPosOrderEvent.order.items.Add(posI);
                }
                foreach (var pmt in o.new_state.payments)
                {
                    sPosOrderEvent.payment_records.Add(new SPosPayment
                    {
                        Amount = Convert.ToDecimal(pmt.amount.Replace(" GBP", string.Empty)),
                        AuthCode = (pmt.info == null) ? "" : pmt.info.pay_id.ToString(),
                        PaymentTime = (o.new_state.created_at == null) ? new DateTime() : (DateTime)o.new_state.created_at,
                        PaymentType = (pmt.type == "cash") ? 0 : 5
                    });
                }

                if (sPosOrderEvent.order.is_paid == true)
                {
                    foreach (var charge in o.new_state.charges)
                    {
                        sPosOrderEvent.order.items.Add(new SPosItem
                        {
                            pos_item_name = charge.charge_type,
                            pos_item_id = 0,
                            quantity = 1,
                            unit_price = (int)(Convert.ToDecimal(charge.charge_price.Replace(" GBP", string.Empty)) * 100),
                            modifiers = new List<SPosModifier>()
                        });
                    }
                }

                #region test_code

                //if (!string.IsNullOrEmpty(o.new_state.loyalty_operations))
                //    sPosOrderEvent.order.items.Add(new SPosItem
                //    {
                //        pos_item_name = "Voucher:" + data.voucher.Code,
                //        pos_item_id = 0,
                //        quantity = 1,
                //        unit_price = 0,
                //        modifiers = new List<SPosModifier>()
                //    });
                //if (data.voucher.Amount != 0 && !string.IsNullOrEmpty(data.voucher.Text))
                //    sPosOrderEvent.order.items.Add(new SPosItem
                //    {
                //        pos_item_name = data.voucher.Text,
                //        pos_item_id = 0,
                //        quantity = 1,
                //        unit_price = (int)(data.voucher.Amount * -100),
                //        modifiers = new List<SPosModifier>()
                //    });

                //if (data.isCardPayment)
                //{
                //    sPosOrderEvent.order.items.Add(new SPosItem
                //    {
                //        pos_item_name = "**** ORDER PAID ONLINE ****",
                //        pos_item_id = 0,
                //        quantity = 1,
                //        unit_price = 0,
                //        modifiers = new List<SPosModifier>()
                //    });
                //}
                //if (data.cart.WebDeliveryCost > 0)
                //{
                //    sPosOrderEvent.order.items.Add(new SPosItem
                //    {
                //        pos_item_name = (data.isCardPayment) ? "Delivery Cost" : "Collection Cost",
                //        pos_item_id = 0,
                //        quantity = 1,
                //        unit_price = (int)(data.cart.WebDeliveryCost * 100),
                //        modifiers = new List<SPosModifier>()
                //    });
                //}
                //sPosOrderEvent.order.items.Add(new SPosItem
                //{
                //    pos_item_name = (data.cart.WebIsDelivery ? "Delivery Time: " : "Collection Time: ") + data.cart.WebOrderTime,
                //    pos_item_id = 0,
                //    quantity = 1,
                //    unit_price = (int)(data.cart.WebDeliveryCost * 100),
                //    modifiers = new List<SPosModifier>()
                //});

                #endregion

                var retVal =  await ServicePost("ProcessOnlineOrder", sPosOrderEvent);
                Log("ProcessOnlineOrder retVal = " + retVal);
                return retVal;
            }
            catch (Exception eeee)
            {
                var ee = eeee.Message;
                Log("****HubRiseProcessOrder-Exception" + eeee.Message);
                return null;
            }
        }

        private static async Task<List<ImageData>> UploadImagesToHubRise(BranchMenu menu, string CompanyName, string catalogId, string token)
        {
            string drive = Environment.SystemDirectory.Substring(0, 2);

            DirectoryInfo imagesDir = new DirectoryInfo(drive + @"\inetpub\wwwroot\images\" + CompanyName.ToUpper());

            try
            {
                if (imagesDir.Exists)
                {
                    var localImagesData = GetLocalImagesData(menu, imagesDir);

                    var hubRiseImagesData = await GetHubRiseImagesData(catalogId, token);

                    if (localImagesData == null || hubRiseImagesData == null) return null;

                    foreach (var iData in localImagesData)
                    {
                        var hrIData = hubRiseImagesData.Find(li => li.md5 == iData.md5);

                        if (hrIData == null)
                        {
                            Dictionary<string, string> headers = new Dictionary<string, string>();
                            headers.Add("Content-Type", iData.type);
                            headers.Add("X-Access-Token", token);

                            var receivedImageData = await ApiClient.CallApi<ImageData>(HUBRISE_BASE_URL, "/catalogs/" + catalogId + "/images", iData.binary, headers, RestApiCallType.POST, true);

                            if (receivedImageData != null)
                            {
                                iData.Id = receivedImageData.Id; // assign image an Id
                            }
                        }
                        else
                        {
                            iData.Id = hrIData.Id;  // update image Id
                        }

                        iData.binary = null; // we dont need binary aymore
                    }
                    return localImagesData;
                }
                return null;
            }
            catch(Exception e)
            {
                Log("****UploadImagesToHubRise-Exception" + e.Message);
                return null;
            }
        }

        private static async Task<List<ImageData>> GetHubRiseImagesData(string catalogId, string token)
        {
            try
            {
                Dictionary<string, string> headers = new Dictionary<string, string>();
                headers.Add("Content-Type", "application/json");
                headers.Add("X-Access-Token", token);

                return await ApiClient.CallApi<List<ImageData>>(HUBRISE_BASE_URL, "/catalogs/" + catalogId + "/images", null, headers, RestApiCallType.GET, false);
            }
            catch { return null; }
        }

        private static List<ImageData> GetLocalImagesData(BranchMenu menu, DirectoryInfo imagesDir)
        {
            List<ImageData> imagesData = new List<ImageData>();

            // Menu images
            foreach (var category in menu.Categories)
            {
                foreach (var product in category.Products)
                {
                    foreach (var imageName in product.ImageUrls)
                    {
                        if (!string.IsNullOrEmpty(imageName))
                        {
                            var fileInfo = imagesDir.GetFiles(imageName);
                            if (fileInfo.Length > 0) // found the file 
                            {
                                var iData = GetImageData(fileInfo);

                                if (iData == null) continue;
                                if (string.IsNullOrEmpty(iData.md5)) continue;

                                imagesData.Add(iData);
                            }
                        }
                    }
                }
            }

            // Deal images
            foreach (var deal in menu.Deals)
            {
                if (!string.IsNullOrEmpty(deal.ImageUrl))
                {
                    var fileInfo = imagesDir.GetFiles(deal.ImageUrl);
                    if (fileInfo.Length > 0) // found the file 
                    {
                        var iData = GetImageData(fileInfo);

                        if (iData == null) continue;
                        if (string.IsNullOrEmpty(iData.md5)) continue;

                        imagesData.Add(iData);
                    }
                }
            }

            return imagesData;
        }

        private static ImageData GetImageData(FileInfo[] fileInfo)
        {
            if (fileInfo == null) return null;
            if (fileInfo.Length <= 0) return null;

            ImageData iData = new ImageData
            {
                name = fileInfo[0].Name
            };

            switch (fileInfo[0].Extension)
            {
                case ".jpg":
                    iData.type = "image/jpeg";
                    break;
                case ".jpeg":
                    iData.type = "image/jpeg";
                    break;
                case ".jpe":
                    iData.type = "image/jpeg";
                    break;
                case ".gif":
                    iData.type = "image/gif";
                    break;
                case ".png":
                    iData.type = "image/png";
                    break;
                case ".tiff":
                    iData.type = "image/tiff";
                    break;
                default:
                    return null; // probably not an image, ignore this file.
            }

            string md5Hash = GetMd5Hash(fileInfo[0].FullName, ref iData); // updates iData.binary as well

            if (string.IsNullOrEmpty(md5Hash))
                return null; // true, only if file is corrupt

            iData.md5 = md5Hash;

            return iData;
        }

        private static string GetMd5Hash(string filePath, ref ImageData iData)
        {
            try
            {
                using (var md5 = MD5.Create())
                {
                    StringBuilder sb = new StringBuilder();

                    iData.binary = File.ReadAllBytes(filePath);
                    if (iData.binary == null) return null;

                    // serialise and encoding changes md5hash, hence using the same as Api call for parity
                    //var payload = JsonConvert.SerializeObject(iData.binary);
                    //byte[] hashBytes = md5.ComputeHash(Encoding.Default.GetBytes(payload));

                    byte[] hashBytes = md5.ComputeHash(iData.binary);

                    if (hashBytes == null) return null;

                    foreach (byte bt in hashBytes)
                    {
                        sb.Append(bt.ToString("x2"));
                    }

                    return sb.ToString();
                }
            }
            catch { return null; }
        }

        private static async Task<bool> HubRiseUploadCatalog(string deliveryType, string clientID, string companyName)
        {
            try
            {
                using (EposHubDbDataContext dc = new EposHubDbDataContext())
                {
                    string SPosMenuJson = "", HubRiseAccessToken = "", HubRiseMennuID = "";

                    HubRiseAccessToken = (from s in dc.sp_StationsByClientUD(new Guid(clientID)) select s.HubRiseAccessToken).FirstOrDefault();
                    if (deliveryType == "delivery")
                    {
                        SPosMenuJson = (from s in dc.sp_StationsByClientUD(new Guid(clientID)) select s.MenuDelivery).FirstOrDefault();
                        HubRiseMennuID = (from s in dc.sp_StationsByClientUD(new Guid(clientID)) select s.HubRiseMenuDeliveryID).FirstOrDefault();
                    }
                    else if (deliveryType == "collection")
                    {
                        SPosMenuJson = (from s in dc.sp_StationsByClientUD(new Guid(clientID)) select s.MenuCollection).FirstOrDefault();
                        HubRiseMennuID = (from s in dc.sp_StationsByClientUD(new Guid(clientID)) select s.HubRiseMenuCollectionID).FirstOrDefault();
                    }

                    if (string.IsNullOrEmpty(SPosMenuJson) || string.IsNullOrEmpty(HubRiseAccessToken) || string.IsNullOrEmpty(HubRiseMennuID)) return false;

                    var branchMenu = JsonConvert.DeserializeObject<BranchMenu>(JsonConvert.DeserializeObject(SPosMenuJson).ToString());
                    if (branchMenu == null) return false;

                    var imagesDataTask = UploadImagesToHubRise(branchMenu, companyName, HubRiseMennuID, HubRiseAccessToken);
                    var imagesData = imagesDataTask.Result;

                    HubRiseMenu hrMenu = ConvertToHubRiseMenu(branchMenu, imagesData);
                    if (hrMenu == null) return false;

                    var serialisedMenu = JsonConvert.SerializeObject(hrMenu);

                    // // return true;

                    Dictionary<string, string> headers = new Dictionary<string, string>();
                    headers.Add("Content-Type", "application/json");
                    headers.Add("X-Access-Token", HubRiseAccessToken);

                    if (await ApiClient.CallApi<HubRiseMenuResponse>(HUBRISE_BASE_URL, "catalogs/" + HubRiseMennuID, hrMenu, headers, RestApiCallType.PUT, false) == null)
                        return false;

                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private static HubRiseMenu ConvertToHubRiseMenu(BranchMenu branchMenu, List<ImageData> imagesData)
        {
            if (branchMenu == null) return null;

            HubRiseMenu menu = new HubRiseMenu
            {
                data = new HubRiseData
                {
                    categories = new List<HubRiseCategories>(),
                    products = new List<HubRiseProducts>(),
                    option_lists = new List<HubRiseOptionLists>(),
                    discounts = new List<HubRiseDiscounts>(),
                    deals = new List<HubRiseDeal>()
                }
            };

            foreach (var deal in branchMenu.Deals)
            {
                var hrDeal = new HubRiseDeal
                {
                    name = deal.Name,
                    @ref = deal.ID.ToString(),
                    description = deal.Description,
                    image_ids = new List<string>(),
                    lines = new List<HubRiseLine>()
                };

                // Added for validation  - BZK 18/05/04 
                if (imagesData != null)
                {
                    var iData = imagesData.Find(id => id.name == deal.ImageUrl);
                    if (iData != null)
                    {
                        hrDeal.image_ids.Add(iData.Id);
                    }
                }
                foreach (var pGroup in deal.ProductGroups)
                {
                    var line = new HubRiseLine
                    {
                        label = string.IsNullOrWhiteSpace(pGroup.Weblabel) ? pGroup.Name : pGroup.Weblabel,
                        skus = new List<HubRiseLineSku>()
                    };
                    foreach (var set in pGroup.ProductVariants)
                    {
                        line.skus.Add(new HubRiseLineSku
                        {
                            @ref = set.VariantID.ToString()
                        });
                    }

                    for (int i = 0; i < pGroup.Number; i++)
                    {
                        if (pGroup.WeblabelOrder == 1 && i == 0)
                        {
                            line.pricing_effect = "fixed_price";
                            line.pricing_value = deal.Price.ToString("F") + " GBP";
                        }
                        else
                        {
                            line.pricing_effect = "percentage_off";
                            line.pricing_value = "100";
                        }
                    }

                    hrDeal.lines.Add(line);
                }
                menu.data.deals.Add(hrDeal);
            }


            foreach (var category in branchMenu.Categories)
            {
                menu.data.categories.Add(new HubRiseCategories
                {
                    name = category.Name,
                    description = "",
                    @ref = category.CategoryID.ToString(),
                    tags = null
                });
                foreach (var product in category.Products)
                {
                    if (product.ImageUrls == null)
                    {
                        product.ImageUrls = new List<string>();

                    }

                    List<string> imageIds = new List<string>();

                    foreach (var imageName in product.ImageUrls)
                    {
                        // Added for validation  - BZK 18/05/04 
                        if (imagesData != null)
                        {
                            var imageData = imagesData.Find(id => id.name == imageName);
                            if (imageData != null)
                            {
                                imageIds.Add(imageData.Id);
                            }
                        }
                    }

                    var hrProduct = new HubRiseProducts
                    {
                        category_ref = category.CategoryID.ToString(),
                        description = product.Description,
                        image_ids = imageIds,
                        name = product.Name,
                        tags = new List<string>()
                    };

                    if (!string.IsNullOrEmpty(product.AppDescription))
                    {
                        var tags = product.AppDescription.Split(',');
                        if (tags != null)
                        {
                            foreach (var tag in tags)
                                hrProduct.tags.Add(tag);
                        }
                    }

                    hrProduct.skus = new List<HubRiseSku>();
                    int posCounter = 0;
                    foreach (var sku in product.Sizes)
                    {
                        posCounter++;
                        var hrSku = new HubRiseSku
                        {
                            name = sku.Text,
                            @ref = sku.VariantID.ToString(),
                            position = posCounter,
                            price = sku.Price.ToString("F") + " GBP",
                            option_list_refs = null
                        };

                        hrSku.option_list_refs = new List<string>();

                        var Toppings = new HubRiseOptionLists
                        {
                            name = "Toppings",
                            @ref = product.Name + " " + sku.Text + " Toppings",
                            type = "multiple",
                            options = new List<HubRiseOption>()
                        };
                        var Ingredients = new HubRiseOptionLists
                        {
                            name = "Ingredients",
                            @ref = product.Name + " " + sku.Text + " Ingredients",
                            type = "multiple",
                            options = new List<HubRiseOption>()
                        };
                        var Options = new HubRiseOptionLists
                        {
                            name = "Options",
                            @ref = product.Name + " " + sku.Text + " Options",
                            type = "multiple",
                            options = new List<HubRiseOption>()
                        };
                        var SingularList = new List<HubRiseOptionLists>();

                        foreach (var option in product.Modifiers)
                        {
                            WebListItem mod = null;
                            if (option.Sizes == null) continue;
                            foreach (var size in option.Sizes)
                            {
                                if (size.ItemID == sku.ItemID)
                                {
                                    mod = size;
                                    break;
                                }
                            }

                            if (option.Header.ToLower().Contains("ingredients"))
                                option.Header = "ingredients";
                            switch (option.Header.ToLower())
                            {
                                case "ingredients":
                                    Ingredients.options.Add(new HubRiseOption
                                    {
                                        name = option.Text,
                                        @ref = (mod == null) ? option.ItemID.ToString() : mod.VariantID.ToString()
                                    });
                                    break;
                                case "options":
                                    Options.options.Add(new HubRiseOption
                                    {
                                        name = option.Text,
                                        price = (mod == null) ? option.Price.ToString("F") + " GBP" : mod.Price.ToString("F") + " GBP",
                                        @ref = (mod == null) ? option.ItemID.ToString() : mod.VariantID.ToString(),
                                        tags = new List<string> { option.Header.ToString() }
                                    });
                                    break;
                                default:
                                    if (option?.ForceNumber == 1)
                                    {
                                        var singular = SingularList.Find(l => l.@ref == product.Name + " " + sku.Text + " " + option.Header);
                                        if (singular == null)
                                        {
                                            singular = new HubRiseOptionLists
                                            {
                                                name = option.Header,
                                                @ref = product.Name + " " + sku.Text + " " + option.Header,
                                                type = "single",
                                                options = new List<HubRiseOption>()
                                            };
                                            singular.options.Add(new HubRiseOption
                                            {
                                                name = option.Text,
                                                price = (mod == null) ? option.Price.ToString("F") + " GBP" : mod.Price.ToString("F") + " GBP",
                                                @ref = (mod == null) ? option.ItemID.ToString() : mod.VariantID.ToString()
                                            });
                                            SingularList.Add(singular);
                                        }
                                        else
                                        {
                                            if (singular.options == null) // can't be
                                                singular.options = new List<HubRiseOption>();
                                            singular.options.Add(new HubRiseOption
                                            {
                                                name = option.Text,
                                                price = (mod == null) ? option.Price.ToString("F") + " GBP" : mod.Price.ToString("F") + " GBP",
                                                @ref = (mod == null) ? option.ItemID.ToString() : mod.VariantID.ToString()
                                                // tag not needed, i think?
                                            });
                                        }
                                    }
                                    else // Toppings
                                    {
                                        Toppings.options.Add(new HubRiseOption
                                        {
                                            name = option.Text,
                                            price = (mod == null) ? option.Price.ToString("F") + " GBP" : mod.Price.ToString("F") + " GBP",
                                            @ref = (mod == null) ? option.ItemID.ToString() : mod.VariantID.ToString(),
                                            tags = new List<string> { option.Header.ToString() }
                                        });
                                    }
                                    break;
                            }
                        }

                        // adding SKU references
                        if (Toppings?.options?.Count > 0)
                        {
                            hrSku.option_list_refs.Add(Toppings.@ref);
                            menu.data.option_lists.Add(Toppings);
                        }
                        if (Ingredients?.options?.Count > 0)
                        {
                            hrSku.option_list_refs.Add(Ingredients.@ref);
                            menu.data.option_lists.Add(Ingredients);
                        }
                        if (Options?.options?.Count > 0)
                        {
                            hrSku.option_list_refs.Add(Options.@ref);
                            menu.data.option_lists.Add(Options);
                        }
                        foreach (var singular in SingularList)
                        {
                            hrSku.option_list_refs.Add(singular.@ref);
                            menu.data.option_lists.Add(singular);
                        }
                        hrProduct.skus.Add(hrSku);
                    }

                    menu.data.products.Add(hrProduct);
                }
            }

            return menu;
        }

        public static void Log(string text)
        {
            try
            {
                // log exception
                string drive = Environment.SystemDirectory.Substring(0, 2);
                DirectoryInfo exceptionDir = new DirectoryInfo(drive + @"\EposHubEventsLogs");

                if (!exceptionDir.Exists)
                {
                    exceptionDir.Create();
                }

                TextWriter writer = new StreamWriter(drive + @"\EposHubEventsLogs\EposHubBgProcess " + DateTime.Now.ToLongDateString() + ".txt", true);
                writer.WriteLine(DateTime.Now.ToString() + "  -  " + text);
                writer.Close();

                System.Diagnostics.Debug.WriteLine(text);
            }
            catch
            { }
        }


    }
}
