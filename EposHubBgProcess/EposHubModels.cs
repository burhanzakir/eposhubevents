﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EposHubBgProcess
{
    public class SPosModifier
    {
        public int pos_item_id { get; set; }
        public int unit_price { get; set; }
        public string mod_name { get; set; }
    }
    public class SPosItem
    {
        public string pos_item_name { get; set; }
        public int pos_item_id { get; set; }
        public int unit_price { get; set; }
        public int quantity { get; set; }
        public List<SPosModifier> modifiers { get; set; }
    }
    public class SPosOrder
    {
        public string order_id { get; set; }
        public string name { get; set; }
        public string order_type { get; set; }
        public string asap { get; set; }
        public string pickup_at { get; set; }
        public int total { get; set; }
        public string currency { get; set; }
        public string notes { get; set; }
        public List<SPosItem> items { get; set; }
        public string OrderJSON { get; set; }
        public bool? is_paid { get; set; }
        public bool? is_collection { get; set; }
    }
    public class SPosPayment
    {
        public int PaymentType { get; set; }
        public decimal Amount { get; set; }
        public string AuthCode { get; set; }
        public DateTime PaymentTime { get; set; }
    }
    public class SPosOrderEvent
    {
        public string @event { get; set; }
        public string acknowledged_at { get; set; }
        public string location_id { get; set; }
        public SPosOrder order { get; set; }
        public OnlineCustomer customer { get; set; }
        public List<SPosPayment> payment_records { get; set; }
    }
    public class OnlineCustomer
    {
        public Customer Customer { get; set; }
        public CustomerAddress Address { get; set; }
    }
}
