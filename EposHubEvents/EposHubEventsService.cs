﻿using EposHubBgProcess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace EposHubEvents
{
    public partial class EposHubEventsService : ServiceBase
    {
        EposHubBgProcessController _EposHubBgProcessController;

        public EposHubEventsService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            _EposHubBgProcessController = new EposHubBgProcessController();
        }

        protected override void OnStop()
        {
        }
    }
}
