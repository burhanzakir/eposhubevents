﻿/*------------------------------------------------------------------------------------------
 *  
 *  ProjectIntaller.cs
 *  
 *  Project: EPOS Software Solution Systems
 *  
 *  Creation Date: 24th November 2009
 *  
 *  Written By: Tim Daughton
 * 
 *  Summary: Servce installer
 *           
 *  Inherits From: Installer
 *  
 *------------------------------------------------------------------------------------------
 */
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;
using System.IO;
using System.Security.AccessControl;
using System.Configuration;
using System.ServiceModel;
using System.ServiceModel.Configuration;


namespace SoftwareSolutionSystems.Epos.Services
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }

        private void ProjectInstaller_AfterInstall(object sender, InstallEventArgs e)
        {
            // start service
            try
            {
                //Log("After Install Called");

                ServiceController controller = new ServiceController("EposHubEventsService");

                if (controller.Status == ServiceControllerStatus.Stopped)
                {
                    controller.Start();
                }

                //Log("Service Started");

                //System.Windows.Forms.MessageBox.Show("After Install");
                //System.Windows.Forms.MessageBox.Show("After Install path: " + Context.Parameters["targetdir"]);

                //CustomInstaller inst = new CustomInstaller(@"C:\Program Files (x86)\Software Solution Systems\Epos 2010");
                //inst.CopyConfig("Epos_2010_svs.exe");

            }
            catch
            { }
        }

        /*private void ProjectInstaller_BeforeInstall(object sender, InstallEventArgs e)
        {
            try
            {
                Log("Before Install Called");

                //System.Windows.Forms.MessageBox.Show("Before Install");
                //System.Windows.Forms.MessageBox.Show("Before Install path: " + Context.Parameters["targetdir"]);

                CustomInstaller inst = new CustomInstaller(@"C:\Program Files (x86)\Software Solution Systems\Epos 2010");

                inst.SetInstallFolderPermissions();
                inst.RenameConfigFile("Epos_2010_svs.exe.config");
            }
            catch
            { }
        }

        public override void Install(IDictionary stateSaver)
        {
            Log("Before Upgrade");
            base.Install(stateSaver);
            Log("After Upgrade");
        }

      

        private void Log(string text)
        {
            try
            {
                // log exception
                string drive = Environment.SystemDirectory.Substring(0, 2);
                DirectoryInfo exceptionDir = new DirectoryInfo(drive + @"\EposExceptionLog");

                if (!exceptionDir.Exists)
                {
                    exceptionDir.Create();
                }

                TextWriter writer = new StreamWriter(drive + @"\EposExceptionLog\SetupLog " + DateTime.Now.ToLongDateString() + ".txt", true);
                writer.WriteLine(DateTime.Now.ToString() + "  -  " + text);
                writer.Close();

                System.Diagnostics.Debug.WriteLine(text);
            }
            catch
            { }
        }
    }

    public class CustomInstaller
    {
        private string _tillInstallPath;

        public CustomInstaller(string tillInstallPath)
        {
            _tillInstallPath = tillInstallPath;
        }

        public void SetInstallFolderPermissions()
        {
            DirectoryInfo myDirectoryInfo = new DirectoryInfo(_tillInstallPath);

            if (myDirectoryInfo.Exists)
            {
                DirectorySecurity myDirectorySecurity = myDirectoryInfo.GetAccessControl();
                myDirectorySecurity.AddAccessRule(new FileSystemAccessRule("Users", FileSystemRights.FullControl, InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                myDirectoryInfo.SetAccessControl(myDirectorySecurity);
            }
        }

        public void RenameConfigFile(string exeFilename)
        {
            DirectoryInfo myDirectoryInfo = new DirectoryInfo(_tillInstallPath);

            if (myDirectoryInfo.Exists)
            {
                string sourceFilename = _tillInstallPath + "\\" + exeFilename;
                string name = Path.GetFileNameWithoutExtension(sourceFilename);
                string targetFilename = sourceFilename.Replace(name, name + "-old");

                // copy exe
                FileInfo exeFile = new FileInfo(sourceFilename);

                if (exeFile.Exists)
                {
                    exeFile.CopyTo(targetFilename, true);
                }

                sourceFilename += ".config";
                targetFilename += ".config";

                // move config exe
                FileInfo configFile = new FileInfo(sourceFilename);

                if (configFile.Exists)
                {
                    configFile.MoveTo(targetFilename);
                }

            }
        }

        public void CopyConfig(string exeFilename)
        {
            string targetFilename = _tillInstallPath + "\\" + exeFilename;
            string name = Path.GetFileNameWithoutExtension(targetFilename);
            string sourceFilename = targetFilename.Replace(name, name + "-old");

            FileInfo sourceFile = new FileInfo(sourceFilename);
            FileInfo targetFile = new FileInfo(targetFilename);

            if (sourceFile.Exists && targetFile.Exists)
            {
                Configuration sourceConfig = ConfigurationManager.OpenExeConfiguration(sourceFilename);
                Configuration targetConfig = ConfigurationManager.OpenExeConfiguration(targetFilename);

                // copy any connection strings

                foreach (ConnectionStringSettings connectionString in sourceConfig.ConnectionStrings.ConnectionStrings)
                {
                    if (targetConfig.ConnectionStrings.ConnectionStrings[connectionString.Name] != null)
                    {
                        targetConfig.ConnectionStrings.ConnectionStrings[connectionString.Name].ConnectionString = connectionString.ConnectionString;
                    }
                }

                // copy any settings

                foreach (ConfigurationSectionGroup sectionGroup in sourceConfig.SectionGroups)
                {
                    if (targetConfig.SectionGroups[sectionGroup.Name] != null)
                    {
                        foreach (ConfigurationSection section in sectionGroup.Sections)
                        {
                            if (targetConfig.SectionGroups[sectionGroup.Name].Sections[section.SectionInformation.Name] != null)
                            {
                                ClientSettingsSection settingSection = (ClientSettingsSection)section;
                                ClientSettingsSection targetSettingSection = (ClientSettingsSection)targetConfig.SectionGroups[sectionGroup.Name].Sections[section.SectionInformation.Name];

                                foreach (SettingElement sourceSetting in settingSection.Settings)
                                {

                                    SettingElement targetSetting = targetSettingSection.Settings.Get(sourceSetting.Name);

                                    if (targetSetting != null)
                                    {
                                        targetSetting.Value.ValueXml.InnerXml = sourceSetting.Value.ValueXml.InnerXml;
                                        targetSettingSection.SectionInformation.ForceSave = true;
                                    }
                                }
                            }
                        }
                    }
                }

              

                // copy and endpoint addresses

                ClientSection sourceClientSection = sourceConfig.GetSection("system.serviceModel/client") as ClientSection;

                if (sourceClientSection != null)
                {
                    ChannelEndpointElementCollection sourceEndpointCollection = sourceClientSection.ElementInformation.Properties[string.Empty].Value as ChannelEndpointElementCollection;

                    if (sourceEndpointCollection != null)
                    {
                        ClientSection targetClientSection = targetConfig.GetSection("system.serviceModel/client") as ClientSection;

                        if (targetClientSection != null)
                        {
                            ChannelEndpointElementCollection targetEndpointCollection = targetClientSection.ElementInformation.Properties[string.Empty].Value as ChannelEndpointElementCollection;

                            foreach (ChannelEndpointElement sourceEndpointElement in sourceEndpointCollection)
                            {
                                foreach (ChannelEndpointElement targetEndpointElement in targetEndpointCollection)
                                {
                                    if (sourceEndpointElement.Name == targetEndpointElement.Name)
                                    {
                                        targetEndpointElement.Address = sourceEndpointElement.Address;
                                    }
                                }
                            }
                        }
                    }
                }

                // save config
                targetConfig.Save();
            }
        }*/
    }

}
